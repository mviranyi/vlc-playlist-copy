<?php

// Copies over all local media files from a VLC playlist file to the specified target folder, using the playlist file's location element.
// USE: php VlcPlaylistCopy.php playlistfile.xplf /target/folder/

// History:
// Wrote on way to Freiburg in ICE because couldn't efficiently copy files to my phone from VLC.

// Show help
if (count($argv) < 3) {
	echo "\n";
	echo "php VlcPlaylistCopy.php playlistfile.xplf /target/folder/\n";
	echo "\n";
	die("Please use as shown, bye bye!\n");
}

function echoLine () {
	echo "----------------------------------------------------------------------\n";
}

// Define target path
// Fixes trailing slash issue

$targetFolder = $argv[2];
if (substr( $targetFolder, -1) != "/") { $targetFolder .= "/"; }

echo "Target folder: ".$targetFolder."\n";

// Open playlist file
// Checks for existance

$playlistFilepath = $argv[1];
if (!file_exists($playlistFilepath)) die("Playlist file doesn't exist!");
$pltWorking = file($playlistFilepath);

echo "Playlist file: ".$playlistFilepath."\n";

// Filters out uninteresting lines
// Only local files are used

$lstWorking = array();
foreach ($pltWorking as $line) {
	// Using file: should filter out folders and remote elements
	if (substr_count($line,"<location>file:") == 1) {
		array_push($lstWorking,$line);
	}
}

echoLine();
echo "Found ".count($lstWorking)." entries in playlist file.\n";
echoLine();

// Remove unwanted stuff

$remove = array(
"<location>file://",
"</location>"
);

$search  = array(
"&#39;"
);
$replace = array(
"'"
);

for ($i=0; $i < count($lstWorking); $i++) {
	$lstWorking[$i] = str_replace($remove,"",$lstWorking[$i]);
	$lstWorking[$i] = urldecode($lstWorking[$i]);
	$lstWorking[$i] = html_entity_decode($lstWorking[$i]);
	$lstWorking[$i] = str_replace($search,$replace,$lstWorking[$i]);
	$lstWorking[$i] = trim($lstWorking[$i]);
}

// Special reaplacements that did not work yet



// Save file to target folder

file_put_contents($targetFolder."Ouput.log",implode("\n",$lstWorking));

// Copy each file to target folder

$done = 0;
$skipped = 0;
$failed = 0;
foreach ($lstWorking as $file) {

	if (!file_exists($file)) {
		echo "Skipping ".$file."!\n";
		$skipped++;
		continue; 
	}

	$filepathParts = explode("/", $file);
	$filename      = $filepathParts[count($filepathParts)-1];

	echo "Copying ".$filename." ..";
	if (@copy($file,$targetFolder.$filename)) {
		echo " Done.\n";
		$done++;
	} else {
		echo " FAILED!!!\n";
		$failed++;
	}
}

// Done

echoLine();
echo "Result\n";
echo "Done: ".$done." Skipped: ".$skipped." Failed: ".$failed."\n";
exit("Bye bye!\n");
